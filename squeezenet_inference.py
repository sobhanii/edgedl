import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import to_categorical

# Load and preprocess the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0
x_test = x_test.reshape(-1, 28, 28, 1)
y_test = to_categorical(y_test)

# Load the saved Keras model
loaded_model = tf.keras.models.load_model('squeezenet_model.h5')

# Perform inference using the loaded model
predictions = loaded_model.predict(x_test)

# Calculate accuracy
accuracy = tf.keras.metrics.CategoricalAccuracy()(y_test, predictions).numpy()
print(f"Accuracy: {accuracy * 100:.2f}%")
