import tensorflow as tf
from tensorflow.keras.datasets import mnist
import numpy as np

# Load the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(-1, 28, 28, 1).astype(np.float32)  # Reshape and cast to float32
y_test = y_test.astype(np.int32)  # Cast labels to int32

# Load the saved model for inference
loaded_model = tf.keras.models.load_model('lenet5_model.h5')

# Evaluate the model on the test data
loss, accuracy = loaded_model.evaluate(x_test, y_test, verbose=0)
print(f'Normal Model Accuracy: {accuracy * 100:.2f}%')
