import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.datasets import mnist

# Load the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(-1, 28, 28, 1)  # Reshape data

# Load the saved model
model = load_model('simple_squeezenet_model.h5')

# Perform inference on the test data
predictions = model.predict(x_test)
predicted_labels = np.argmax(predictions, axis=1)

# Calculate accuracy
accuracy = np.mean(np.equal(predicted_labels, y_test)) * 100
print(f'Model Accuracy on Test Data: {accuracy:.2f}%')
